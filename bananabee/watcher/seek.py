"""
Handle the never-ending search for dots within the realm of time.
"""
import time
import uuid
import config
import logging
import datetime
import os
import numpy as np
from PIL import Image
import colorsys
import mss
import mss.tools

from collections import namedtuple

LOGGER = logging.getLogger(__name__)

Point = namedtuple('Point', 'x,y,label')
PixelRGB = namedtuple('PixelRGB', 'r,g,b,label')

REFERENCES = [
  PixelRGB(255, 0, 0, 'RED'),
  PixelRGB(0, 255, 0, 'GREEN'),
  PixelRGB(19, 23, 34, 'BACKGROUND'),
]

POINTS_TO_CHECK = [
  Point(*(config.PARAMS['watcher']['point_dot'] + ['DOT'])),
  Point(*(config.PARAMS['watcher']['point_bar'] + ['BAR'])),
  Point(*(config.PARAMS['watcher']['point_timer'] + ['TIMER'])),
]

SIMPLE_GUESS_LOG = True

# Pixels around the image that will be screenshotted with the point of interest
PATCH_SIZE = 20

# Enable Debug mode
DEBUG = False

if DEBUG and not os.path.isdir('screenshots'):
  os.mkdir('screenshots')

def timestamp():
  return datetime.datetime.now().isoformat()

def calc_monitor_patch(point : Point):
  """Calc the sub-area to screenshot for this point."""
  return {
    'left': point.x - PATCH_SIZE,
    'top': point.y - PATCH_SIZE,
    'width': 2 * PATCH_SIZE,
    'height': 2 * PATCH_SIZE
  }

SCT = mss.mss()

def get_pixel_at(point : Point) -> (int, int, int):
  """Get pixel at a specific point"""
  sct_img = SCT.grab(calc_monitor_patch(point))
  img = Image.frombytes('RGB', sct_img.size, sct_img.bgra, 'raw', 'BGRX')
  if DEBUG:
    filename = f'screenshots/swatch-{point.x}-{point.y}-{timestamp()}.png'
    LOGGER.debug(f'Saving to {filename}')
    img.save(filename)
  return img.getpixel((PATCH_SIZE, PATCH_SIZE))


def unit_vector_length(pixel, reference):
  """Return the dot product of the pixel and the reference pixel."""
  return np.dot(pixel, reference[:3]) / (np.linalg.norm(pixel) * np.linalg.norm(reference[:3]))


def make_prediction(pixel, references):
  """Predict which reference matches the pixel best."""
  current_high = unit_vector_length(pixel, references[0])
  current_high_reference = references[0]
  for reference in references[1:]:
    value = unit_vector_length(pixel, reference)
    if value > current_high:
      current_high, current_high_reference = value, reference
  return current_high_reference, current_high


def guess_pixel_color_at(point : Point) -> (PixelRGB, float):
  """Guess the Pixel color of a point on the screen"""
  pixel = get_pixel_at(point)
  guess, confidence = make_prediction(pixel, REFERENCES)
  if False:
    LOGGER.debug(f'Point({point.x}, {point.y}, \'{point.label}\') ~ {guess.label} ({confidence*100:.3f}% similar)')
  return guess, confidence


def get_pixel_guesses_gen():
  """Fetch the guesses for all the points on the screen."""
  for point in POINTS_TO_CHECK:
    guess, confidence = guess_pixel_color_at(point)
    yield point, guess, confidence

def get_pixel_guesses():
  """Fetch the guesses for all the points on the screen."""
  return list(get_pixel_guesses_gen())

def evaluate_pixels(state, queue):
  pixels = get_pixel_guesses()
  LOGGER.debug(f'Pixels: {pixels}')
  queue.put(pixels)
  return pixels

def init(state, queue):
  while True:
    evaluate_pixels(state, queue)
    time.sleep(config.PARAMS['watcher']['period'])


def start(error_queue, state, queue):
  """Start this process"""
  LOGGER.info('Initializing watcher process')
  return init(state, queue)