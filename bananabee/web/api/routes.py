from flask import (
  Blueprint,
  jsonify,
)


API = Blueprint('api', __name__)


@API.route('/version')
def version():
  return jsonify(**{'version': '0.1.0'})
