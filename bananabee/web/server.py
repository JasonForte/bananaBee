"""
Serve the Web Interface from Python
"""
import logging

from flask import (
  Flask,
  jsonify,
  send_from_directory
)

from flask_cors import CORS

from .api.routes import API

LOGGER = logging.getLogger(__name__)


APP = Flask(__name__, static_folder='dist')
APP.register_blueprint(API, url_prefix='/api')
CORS(APP)


@APP.route('/', defaults={'path': 'index.html'})
@APP.route('/<path:path>')
def main(path):
  return send_from_directory('dist', path)


def start(error_queue, state):
  """Start the Flask web server"""
  try:
    return APP.run(host='0.0.0.0', port=4040, debug=False)
  except:
    LOGGER.info('Exiting server process')
    error_queue.put(-1)


