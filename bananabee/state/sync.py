"""
Handle synchronization of the Bot with the watcher.
"""
import asyncio
import logging
import time
import datetime
import config

import pytoml as toml

LOGGER = logging.getLogger(__name__)

#
# How many timing bars need to pass to be considered "synced"
#
MIN_SYNC_BARS = config.PARAMS['control']['sync_bars']

THEORETICAL_PERIOD_TABLE = {
  '1m': 60,
  '2m': (60 * 2),
  '3m': (60 * 3),
  '5m': (60 * 5),
  '15m': (60 * 15)
}

THEORETICAL_PERIOD = THEORETICAL_PERIOD_TABLE[config.PARAMS['tradingview']['period']]
TRADING_WINDOW_SECONDS = config.PARAMS['tradingview']['trading_window_seconds']


class BotSync:

  def __init__(self, init_time, freq_theory=None):
    # The theoretical frequency of this chart
    self.freq_theory = freq_theory or (1 /  THEORETICAL_PERIOD)
    self.freq = freq_theory or (1 / THEORETICAL_PERIOD)
    self.current_time = 0
    self.last_column_time = 0
    self.current_tick_color = None
    self.last_tick_color = None
    self.freq_mult = 1
    self.column_period_total = 0
    self.period_list = []
    self.last_store_time = 0

  def store_overdue(self):
    return (self.current_time - self.last_store_time) > 10

  def record_storage(self):
    LOGGER.debug(f'Recorded storage operation at: {self.current_time}')
    self.last_store_time = self.current_time

  def update_timer_color(self, color):
    """Update the current tick color"""
    if self.current_tick_color:
      self.last_tick_color = self.current_tick_color
    self.current_tick_color = color
    LOGGER.debug(f'Updated tick color to {self.current_tick_color.label}')

  def touch(self):
    """Update the """
    self.current_time = time.time()
    LOGGER.debug(f'Updated time to: {self.current_time}')
    if self.freq and self.freq_mult:
      LOGGER.debug(f'Frequency adjusted to: {self.freq:.4f} Hz')
      LOGGER.debug(f'Frequency multiplier: {self.freq_mult:.4f}')

  def perform_transition_to_column(self):
    """What happens when a new column is detected?"""
    if not self.last_column_time:
      self.last_column_time = self.current_time
      LOGGER.debug(f'Last column time set to: {self.last_column_time}')
      return None
    self.period_list.append(self.current_time - self.last_column_time)
    self.period_list = self.period_list[-10:]
    LOGGER.debug(f'{self.period_list}')
    self.freq = len(self.period_list) / sum(self.period_list)
    LOGGER.info(f'Frequency adjusted to: {self.freq:.3f} Hz')
    self.freq_mult = self.freq / self.freq_theory
    LOGGER.info(f'Frequency multiplier: {self.freq_mult:.3f} Hz')
    self.last_column_time = self.current_time

  def next_tick(self):
    """Return when the next tick will be."""
    next_tick = self.last_column_time + 1 / (self.freq_mult * self.freq_theory)
    LOGGER.debug(f'Next Tick: {next_tick}')
    return next_tick

  def time_remaining(self):
    time_remaining = (self.next_tick() - self.current_time) % THEORETICAL_PERIOD
    LOGGER.info(f'Time remaining: {time_remaining:.0f} s')
    return time_remaining

  def trading_window_open(self):
    time_remaining = self.time_remaining()
    if not time_remaining:
      return False
    return time_remaining < TRADING_WINDOW_SECONDS

  def is_synced(self):
    return len(self.period_list) >= MIN_SYNC_BARS

  def check_for_new_column(self):
    """Check if a new column has appeared"""
    if not self.last_tick_color or not self.current_tick_color:
      LOGGER.debug('Catching up tick colors...')
      return None

    if self.last_tick_color.label == 'BACKGROUND' and self.current_tick_color.label == 'RED':
      self.perform_transition_to_column()
