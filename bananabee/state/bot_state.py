"""
Represent the global state of the bot.
"""
import json
import logging
import os
import time
import uuid

import colorama

from multiprocessing import Lock

from bananabee.state.sync import BotSync


STATE_DIR = './.state'

# Create the directory for state if it doesn't exist.
if not os.path.isdir(STATE_DIR):
  os.mkdir(STATE_DIR)

LOGGER = logging.getLogger(__name__)


class BotState:
  """
  Represents the state of the Trading Bot
  """

  lock = Lock()

  def __init__(self, reference=None):
    self.reference = reference or str(uuid.uuid4())
    self._raw_state = dict()
    self.retain_history = 6
    self._state = {
      'history': {},
      'current': {},
      'previous': {}
    }
    self.committed = False
    self.committed_time = 0.0
    self.balance = {'balance': 0.0, 'loginid': '', 'currency': ''}
    self.next_action = 'STAY'
    self.sync = BotSync(time.time())
    # Storage for the strategy
    self.store = dict()
    self.strategy = None
    self.stake = 0.0

  def ensure_state(self):
    filename = os.path.join(STATE_DIR, f'{self.reference}.json')
    if not os.path.isfile(filename):
      with open(filename, 'w') as fp:
        json.dump({}, fp)

  @classmethod
  def from_reference(cls, reference=None):
    """Load the bot state from a reference or create a new bot state."""
    if reference:
      instance = cls(reference)
      # TODO: <hello@jforte.me> Ensure that the storage object is initialized here if needed.
      instance.load()
      return instance
    return cls()

  @property
  def dot(self):
    return self._state['current']['DOT']

  @property
  def bar(self):
    return self._state['current']['BAR']

  @property
  def timer(self):
    return self._state['current']['TIMER']

  def load(self):
    self.ensure_state()
    filename = os.path.join(STATE_DIR, f'{self.reference}.json')
    LOGGER.info(f'Loading state: {self.reference}')
    with BotState.lock, open(filename, 'r') as fp:
      self.parse(json.load(fp))

  def save(self):
    filename = os.path.join(STATE_DIR, f'{self.reference}.json')
    with BotState.lock, open(filename, 'w') as fp:
      json.dump(self.serialize(), fp)

  def serialize(self):
    """Serialize this state object for saving."""
    return {'balance': self.balance}

  def parse(self, value):
    """Serialize this state object for saving."""
    self._raw_state = value
    balance = self._raw_state.get('balance')
    if balance:
      self.balance = balance

  def _sync_time(self):
    """Synchronize with the timer bars."""
    # Update the current time on the state
    self.sync.touch()

    # Update the current color seen on the timer point
    self.sync.update_timer_color(self.timer)

    # Check if there is a new column
    self.sync.check_for_new_column()

  def append_signals(self, signals):
    """Append a newly received signal to update the state."""
    # Archive the existing value
    current_time = time.time()
    self._state['history'][current_time] = self._state['current']
    self._state['previous'][current_time] = self._state['current']
    for signal in signals:
      point, pixel, _confidence = signal
      self._state['current'][point.label] = pixel
    self._remove_old_history()
    self._sync_time()

  def _remove_old_history(self):
    while len(self._state['history']) > self.retain_history:
      lowest = min(self._state['history'].keys())
      del self._state['history'][lowest]

  def __str__(self):
    return (
      f"{self.balance['loginid']} - "
      + f"Balance: {self.balance['balance']} {self.balance['currency']} Current Stake: {self.stake:0.3f}")
