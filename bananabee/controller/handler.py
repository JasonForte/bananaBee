"""
Main Logic Controller for the bot
"""
import config
import datetime
import logging
import time
import zmq

import colorama

from bananabee.state.bot_state import BotState
from bananabee.controller.strategies import load_available_strategy


LOGGER = logging.getLogger(__name__)


def init(reference=None):
  """Initialize the state of the bot."""
  LOGGER.debug('Initializing the state of the Bot...')
  return BotState.from_reference(reference)


def invoke_create_strategy_store(state : object, config : object, strategy : object, *args, **kwargs) -> dict:
  return strategy.create_strategy_store(state, config)


def invoke_initial_stake(state : object, config: object, strategy : object, *args, **kwargs) -> object:
  return strategy.initial_stake(state, config)


def invoke_evaluate_event(state : object, event : object, strategy : object, *args, **kwargs) -> object:
  return strategy.evaluate_event(state, event, config)


def invoke_evaluate_post_action(state : object, strategy : object, *args, **kwargs) -> object:
  return strategy.evaluate_post_action(state, config)


def set_committed_state(state, committed):
  if config.PARAMS['control']['committed_time']:
    state.committed = committed
    state.committed_time = time.time()
  return state


def perform_action(state : object, action_queue):
  """
  After the state has been evaluated, this function will be called to execute the buy / sell on binary.com
  """
  LOGGER.info(f'[{colorama.Fore.MAGENTA}ACTION{colorama.Fore.RESET}] {state.next_action}')

  if state.next_action == 'UP':
    state = set_committed_state(state, True)
    # Perform CALL if it's going up
    # state.exchange.perform_call(state.stake)
    action_queue.put({'action': 'call', 'stake': state.stake, 'state': state.reference})
    # state.exchange.perform_call(5)

  if state.next_action == 'DOWN':
    state = set_committed_state(state, True)
    # Perform PUT if it's going down
    action_queue.put({'action': 'put', 'stake': state.stake, 'state': state.reference})
    # state.exchange.perform_call(5)
    # state.exchange.perform_put(state.stake)

  state.next_action = 'STAY'
  return state


def commit_state(state):
  state.save()
  return state


def trading_open(state):
  current_time = datetime.datetime.now().time()
  start_time = datetime.datetime.strptime(config.PARAMS['control']['start_time'], "%H:%M").time()
  end_time = datetime.datetime.strptime(config.PARAMS['control']['end_time'], "%H:%M").time()

  if not start_time < current_time < end_time:
    LOGGER.warning('Trading is not open...')
    return None

  return state


def perform_pass(state, signal, action_queue, strategy):
  LOGGER.debug(f'State object: {id(state)}')
  LOGGER.info(f"{colorama.Fore.CYAN}STATE{colorama.Fore.RESET} - {state}")

  # If trading isn't open
  if not trading_open(state):
    state.load()
    return state

  state.append_signals(signal)

  state = invoke_evaluate_event(state, signal, strategy)
  state = perform_action(state, action_queue)
  state = invoke_evaluate_post_action(state, strategy)
  state.load()

  return state


def start(state, signal_queue, action_queue, *args, **kwargs):
  strategy_name = config.PARAMS['control']['strategy']
  LOGGER.info(f'Loading strategy: {strategy_name}')
  strategy = load_available_strategy(strategy_name)

  state = invoke_create_strategy_store(state, config, strategy)
  state = invoke_initial_stake(state, config, strategy)

  while True:
    LOGGER.debug('Waiting for event from the watcher')
    signal = signal_queue.get()
    LOGGER.debug(f'Signal received from the signal watcher: {signal}')
    state = perform_pass(state, signal, action_queue, strategy)
