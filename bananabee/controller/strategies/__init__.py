from bananabee.controller.strategies import fixed_stake
from bananabee.controller.strategies import proportional_stake

AVAILABLE_STRATEGIES = {
  'fixed_stake': fixed_stake,
  'proportional_stake': proportional_stake
}

def load_available_strategy(name):
  try:
    return AVAILABLE_STRATEGIES[name]
  except KeyError:
    available_strategies_list = ', '.join(AVAILABLE_STRATEGIES.keys())
    raise RuntimeError(f'No such strategy {name}. Choose from: {available_strategies_list}')
