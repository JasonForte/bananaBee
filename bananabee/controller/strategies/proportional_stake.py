"""
Implemets a proportional state strategy
"""
import colorama
import logging


LOGGER = logging.getLogger(__name__)


def description():
  return '''
  Always use the same stake no matter the state or outcome.
  '''

#
# Create the initial state that will be used for this strategy
#

def create_strategy_store(state : object, config : object, *args, **kwargs) -> dict:
  state.store = {
    'stake_min': 10,
    'stake_proportion': config.PARAMS['strategy']['proportional_stake']['proportion']
  }
  return state


#
# Initialize the stake at the beginning of the strategy
#

def initial_stake(state : object, config: object, *args, **kwargs) -> object:
  # If balance not set use min. If set use proportion of the balance.
  if state.balance['balance'] == 0.0:
    state.stake = state.store['stake_min']
  else:
    state.stake = state.balance['balance'] * state.store['stake_proportion']
  return state


#
# When an event is triggered from the watcher
#

def evaluate_event(state : object, event : object, *args, **kwargs) -> object:
  """Evaluate the event to determine the next action"""

  if not state.sync.is_synced():
    LOGGER.info('Waiting for sync...')
    state.next_action = 'STAY'
    return state

  if not state.sync.trading_window_open():
    state.next_action = 'STAY'
    LOGGER.info(f'[{colorama.Fore.YELLOW}WAIT{colorama.Fore.RESET}] Trading window {colorama.Fore.RED}closed{colorama.Fore.RESET}')
    return state

  LOGGER.info(f'[{colorama.Fore.YELLOW}WAIT{colorama.Fore.RESET}] Trading window {colorama.Fore.GREEN}open{colorama.Fore.RESET}')

  if state.dot.label == 'GREEN':
    LOGGER.debug(f'[{colorama.Fore.YELLOW}SIGNAL{colorama.Fore.RESET}] Dot signal says - UP')
    state.next_action = 'UP'
    return state

  if state.dot.label == 'RED':
    LOGGER.debug(f'[{colorama.Fore.YELLOW}SIGNAL{colorama.Fore.RESET}] Dot signal says - DOWN')
    state.next_action = 'DOWN'
    return state

  LOGGER.debug(f'[{colorama.Fore.YELLOW}SIGNAL{colorama.Fore.RESET}] Waiting for DOT')
  state.next_action = 'STAY'

  return state


#
# Evaluate Action Result
#
def evaluate_post_action(state : object, *args, **kwargs) -> object:
  return state
