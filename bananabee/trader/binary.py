"""
Updated the Binary Implementation to handle the connections to the API asynchronously.
"""
import asyncio
import json
import logging
import time
import uvloop
import websockets

import config

from multiprocessing import Process, Queue
from bananabee.state.bot_state import BotState

asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())

LOGGER = logging.getLogger(__name__)


class Link:

  def __init__(self, app_id, api_key):
    self.lock = asyncio.Lock()
    self.app_id = app_id
    self.api_key = api_key
    self.touch = False
    self.ws = None

  async def connect(self, app_id):
    self.conn = websockets.connect('wss://ws.binaryws.com/websockets/v3?app_id={}'.format(app_id))

  async def establish(self):
    """Establish connection to the websocket."""
    if self.ws and not self.ws.close_code:
      return None
    LOGGER.info('Attempting to establish a connection to websocket')
    async with self.lock:
      LOGGER.info('Creating connnection...')
      await self.connect(self.app_id)
      self.ws = await self.conn
      LOGGER.info('Authorizing...')
      await self.auth(self.api_key)

  async def auth(self, api_key):
    """Send authentictaion data via the websocket."""
    LOGGER.debug('Sending: authorize')
    result = await self.call({'authorize': api_key})
    if 'error' in result:
      LOGGER.error('Could not establish websocket connection!')
      raise RuntimeError(f'Could not establish websocket connection: {result["error"]["message"]}')
    LOGGER.info('Successfully Connected to Binary API')

  async def call(self, message):
    """Make a call where we expect a response."""
    # Send the message via the websocket
    LOGGER.debug(f'(call) > {message}')
    await self.ws.send(json.dumps(message))

    # Wait for a response on the websocket.
    result = json.loads(await self.ws.recv())
    LOGGER.debug(f'(call) < {result}')
    return result

  async def cast(self, message):
    """Make a cast where we don't expect a response."""
    # Send the message via the websocket
    LOGGER.debug(f'(cast) > {message}')
    await self.ws.send(json.dumps(message))

  async def handle(self, handler):
    """Add a handler for incomming messages"""
    LOGGER.debug('Handling incomming messages using: {}()'.format(handler.__name__))
    async for message in self.ws:
      LOGGER.debug(f'(handle) < {message}')
      await handler(json.loads(message))


class BinaryConfig:
  """
  Object to store the configuration for the Binary API
  """

  def __init__(self):
    """Initiate the Binary interface."""
    self.balance = None
    self.symbol = config.PARAMS['control']['symbol']
    self.duration = 15
    self.duration_unit = 'm'
    self.currency = 'USD'
    self.stake = None


async def retry(func, count=3):
  """Retry the given function count times."""
  LOGGER.info('Sending Request...')
  for i in range(count):
    try:
      return await func()
    except websockets.exceptions.ConnectionClosed:
      LOGGER.info(f'Retrying send - attempts {i+1} ...')
      await asyncio.sleep(1)
      continue
  raise RuntimeError(f'Could not dispatch request after {count} retries')


async def receiver(link, queue_out, state=None):
  """Coroutine to handle inbound messages."""
  LOGGER.info('Initializing Receiver...')
  while True:
    try:
      await link.establish()
      await link.cast({'balance': 1, 'subscribe': 1, 'passthrough': {'state': state.reference}})
      await link.handle(process_received_message)
    except Exception as exc:
      LOGGER.warning('Exception from receiver: {}'.format(exc))
      await asyncio.sleep(5)
      continue


async def dispatch(link, event, retry=3):
  """Dispatch the message after preparing the message and retries"""
  message = await prepare_message(event)

  for i in range(retry):
    try:
      await link.establish()
      return await link.call(message)
    except Exception as err:
      LOGGER.warning(f'Error dispatching message from event: {err}')

  raise RuntimeError(f'Could not dispatch event after {retry} tries.')


async def dispatcher(link, queue_in, queue_out):
  LOGGER.info('Initializing Dispatcher...')
  while True:
    await link.establish()
    event = await queue_in.get()
    raw_response = await dispatch(link, event)
    result = await process_received_message(raw_response)
    await queue_out.put(result)
    queue_in.task_done()


async def source_events(p_queue, queue_in, queue_out):
  """Retrieve events from the process queue and dispatch to the async coroutines."""
  LOGGER.info('Initializing Event Source...')
  while True:
    action = p_queue.get()
    LOGGER.info('Received Action')
    LOGGER.info(f'Action: {action}')

    if action == -1:
      LOGGER.warning('Exiting trader')
      return None

    # A few checks
    if not action:
      LOGGER.warning(f'Invalid action. Skipping execute...')
      continue

    if not config.PARAMS['control']['execute_trades']:
      LOGGER.warning(f'Live trading is not enabled in the config.toml file.')
      continue

    await queue_in.put(action)
    await queue_out.get()
    queue_out.task_done()


#===============================================================================
#   Inbound Handlers
#===============================================================================

async def ping_handler(message, *args, **kwargs):
  LOGGER.info('Successful Ping')
  return message

async def default_handler(message, *args, **kwargs):
  LOGGER.warning(f'Unhandled message response: {message}')
  return message

async def balance_handler(message, *args, **kwargs):
  LOGGER.info('Incomming balance...')
  state = BotState.from_reference(message['passthrough']['state'])
  state.balance = message['balance']
  state.save()
  return message

async def process_received_message(message):
  LOGGER.debug(f'Processed received message: {message}')
  handlers = {
    'ping': ping_handler,
    'balance': balance_handler,
    'default': default_handler
  }

  msg_type = message.get('msg_type', 'default')
  if msg_type not in handlers:
    msg_type = 'default'

  return await handlers[msg_type](message)

#===============================================================================
#   Outbound Handlers
#===============================================================================


async def prepare_default(data, event):
  """Default Message"""
  return event


async def prepare_put(data, event):
  """Prepare Put"""
  binary_config = BinaryConfig()
  data = {
    'buy': 1,
    'parameters': {
      'contract_type': 'PUT',
      'currency': event.get('currency') or binary_config.currency,
      'symbol': event.get('symbol') or binary_config.symbol,
      'duration': event.get('duration') or binary_config.duration,
      'duration_unit': event.get('duration_unit') or binary_config.duration_unit,
      'basis': 'stake',
      'amount': event.get('stake') or binary_config.stake,
    },
    'price': event.get('stake') or binary_config.stake,
    'passthrough': {'state': event.get('state') or None}
  }
  return data

async def prepare_call(data, event):
  """Prepare Call"""
  binary_config = BinaryConfig()
  data = {
    'buy': 1,
    'parameters': {
      'contract_type': 'CALL',
      'currency': event.get('currency') or binary_config.currency,
      'symbol': event.get('symbol') or binary_config.symbol,
      'duration': event.get('duration') or binary_config.duration,
      'duration_unit': event.get('duration_unit') or binary_config.duration_unit,
      'basis': 'stake',
      'amount': event.get('stake') or binary_config.stake,
    },
    'price': event.get('stake') or binary_config.stake,
    'passthrough': {'state': event.get('state') or None}
  }
  return data


async def prepare_message(event):
  LOGGER.debug(f'Preparing message for event: {event}')

  handlers = {
    'put': prepare_put,
    'call': prepare_call,
    'default': prepare_default
  }

  action = event.get('action', 'default')
  if action not in handlers:
    action = 'default'

  data = {}
  return await handlers[action](data, event)


def init(state, queue, *args, **kwargs):
  LOGGER.debug('Initalizing...')

  link_dispatch = Link(config.PARAMS['binary']['app_id'], config.PARAMS['binary']['api_key'])
  link_receive = Link(config.PARAMS['binary']['app_id'], config.PARAMS['binary']['api_key'])

  queue_in = asyncio.Queue()
  queue_out = asyncio.Queue()

  queue.put({'balance': 1, 'passthrough': {'state': state.reference}})

  # # asyncio.ensure_future(init())
  loop = asyncio.get_event_loop()

  asyncio.ensure_future(receiver(link_receive, queue_out, state))
  asyncio.ensure_future(dispatcher(link_dispatch, queue_in, queue_out))
  asyncio.ensure_future(source_events(queue, queue_in, queue_out))

  try:
    loop.run_forever()
  except KeyboardInterrupt as err:
    LOGGER.warning(str(err))


def start(err_queue, *args, **kwargs):
  try:
    return init(*args, **kwargs)
  except Exception as err:
    err_queue.put(str(err))