Work out stake amount
	amount will change/update after every successful trade, "account balance" to be used as basis for working out the levels (see stake_amount)
	can be level 1 or next level of stake_amount if previous contract lost (max 6 levels)
		a lost contract is determined if red rgb gets returned from bar_colour function 15mins after last contract open
		a won contract is determined if green rgb get returned from bar_colour function 15mins after last contract open or account balance increases

https://developers.binary.com/api/#balance

stake_amount:

input var:
    profit_% = 75%
    account_balance = 0.04

level_1: level_1_amount
level_2: (level_1_amount/profit_%)+level_1_amount
level_3: (level_2_amount/profit_%)+level_2_amount
level_4: (level_3_amount/profit_%)+level_3_amount
level_5: (level_4_amount/profit_%)+level_4_amount
level_6: (level_5_amount/profit_%)+level_5_amount

Total of all levels cannot be greater than account_balance
It must work out the new level amounts after every green bar (or at 14:59 perhaps? or get the updated account balance and work it out from there? whichever is quicker
                                                                as a new CALL(green dot) or PUT(red dot) could be needed to be placed as soon as the last contract closes)

if it is first trade of the day, ie, first trade after 2am, it must remember the last level it was on the day before and then carry on from there.
    This is incase it got a few loss contracts the day before and must carry on as if its a continuation, if that makes sense? maybe we can do this in the form of a log file? not sure
