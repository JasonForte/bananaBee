# Binary Trading Bot

## Getting Started - Python3

### Install Python3 and Pipenv

```bash
$ sudo apt install -y python3 python3-pip
$ sudo -H pip3 install pipenv
```

### Install the dependencies for this project

```bash
$ # From the project directory
$ pipenv --python 3
$ pipenv install
```

## Configure Trading View

<img src='./docs/TradingViewLanes.png' width='600' alt='Trading View Lanes'>

The ideal setup for TradingView requires 3 metrics lanes:

1. The timer metric
2. The bar metric
3. The dot metric

**NB** Make sure to press `Alt + R` to reset the view before starting the bot

### Timer metric

The timer creates a bar on every even minute.

To configure the `timer` lane, from the bottom of the screen choose 'Pine Editor'. In the editor window replace the code
with the following:

```javascript
//@version=3
study("Volume Extreme Amplify")

plot(minute % 2 == 0 ? 1 : 0, color=red, style=columns)
```

Then you can choose 'Add to Chart' from the pine editor menu.


### Bar Metric

<img src='./docs/bar-metric.png' width='300' alt='Bar metric'>

Ensure that the bar metric is set to red and green.

### Dot Metric

Ensure that the dot metric is set to red and green.

<img src='./docs/dot-metric.png' width='300' alt='Dot metric'>

> Top Tip: Save this chart layout so that it can be loaded easily in future.

### Configure the Bot

```bash
$ pipenv shell # Enable the virtualenv
(.venv) $ python configure.py
```

### Start the Bot

Once the bot has been configured from the previous step, it can be started.

```bash
(.venv) $ python start.py
```
