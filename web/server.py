import asyncio
import uvloop

from flask import (
  Flask,
  jsonify
)

asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())

APP = Flask(__name__)


@APP.route('/')
def main():
  return jsonify(**{'version': '0.1.0'})


def run_server():
  APP.run(host='0.0.0.0', port=4040)
