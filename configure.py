import pyautogui
import time
import pytoml as toml

import config

CONFIG_OVERRIDE_FILE = 'config.override.toml'

def accept_new_position(section, key, label, wait=5):
  current = config.PARAMS[section][key]
  res = input(
    'Hold mouse near the {} location after pressing enter [c to skip] [{:.0f}, {:.0f}]:'.format(label, *current)
  )
  if res == 'c':
    return
  i = wait
  print(f'Starting countdown ({wait} secs)')
  while i > 0:
    print(f"Counting down: {i}")
    time.sleep(1)
    i -= 1
  position = pyautogui.position()
  print('[SNAP] - Dot location: ({:.0f}, {:.0f})'.format(*position))
  config.PARAMS['watcher'][key] = list(position)
  return position

def accept_new_config(section, key, text, fmt=str):
  res = input(f'{text} [{config.PARAMS[section][key]}]: ')
  if res:
    config.PARAMS[section][key] = fmt(res)


print('Configuring the watcher...')

accept_new_position('watcher', 'point_dot', 'DOT')
accept_new_position('watcher', 'point_bar', 'BAR')
accept_new_position('watcher', 'point_timer', 'TIMER')


print('Configuring binary.com...')

accept_new_config('binary', 'app_id', 'Binary.com App ID')
accept_new_config('binary', 'api_key', 'Binary.com API Key')

print('Writing to {}'.format(CONFIG_OVERRIDE_FILE))
with open(CONFIG_OVERRIDE_FILE, 'w') as fp:
  toml.dump(config.PARAMS, fp, sort_keys=True)

