Place a PUT or CALL contract
	based on dot_colour function
  Green dot: CALL
  Red dot: PUT
  stake amount comes from stake.py
	no new contract to be placed for at least 15mins after contract open
	Contracts to only be placed between 02:00 and 21:00

https://developers.binary.com/api/#buy
https://binary.vanillacommunity.com/discussion/118/api-buy-contract

  params:

buy: 1
price: 0 (it is a required param, but i think its left as 0 becasue of amount param? will need to test)
Optional parameters:
    amount: stake value from stake.py
    basis: stake
    contract_type: CALL or PUT (Call = green dot, Put = red dot)
    currency: BTC (im assuming if thats what my base currency is?)
    duration_unit: 15 mins = m(15) (Duration unit is s(seconds), m(minutes), h(hours), d(days), t(ticks))
    symbol: EURJPY (but can change, lets leave it as is for now)

not sure if other params are required, we can test :)

may get an API error to say: "No further trading is allowed on this contract type for the current trading session." if that does happen then we need to change the symbol type
but i will also need to change the chart to another symbol. So maybe lets code it to stop executing trades if this happens and pop up a notification
