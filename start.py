"""
Initializing MoneyBot
"""
import time
import logging
import colorama

from multiprocessing import Process, Queue

from bananabee.web import server
from bananabee.trader import binary
from bananabee.watcher import seek
from bananabee.controller import handler


LOGGER = logging.getLogger(__name__)


def intro(*args, **kwargs):
  """Print the introductory message on startup."""
  print(f"""
  Welcome to BananaBee
  ---------------------
  Created by: {colorama.Fore.GREEN}Awesomme Company Name{colorama.Fore.RESET}
  """)


def outro(*args, **kwargs):
  print(f"""
  Bye!
  """)


def main():
  """Main program"""

  # Create a queue that sends signals to the controller
  signal_queue = Queue()

  # Create an event queue
  action_queue = Queue()

  # Error Queue
  error_queue = Queue()

  # Initialize the state of the bot
  state = handler.init()

  # Start the user interface
  p1 = Process(name='server', target=server.start, args=(error_queue, state,))
  p1.start()

  # Start the trader API connection manager
  p2 = Process(name='trader', target=binary.start, args=(error_queue, state, action_queue, signal_queue))
  p2.start()

  p3 = Process(name='watcher', target=seek.start, args=(error_queue, state, signal_queue))
  p3.start()

  handler.start(state, signal_queue, action_queue)


if __name__ == "__main__":
  # Print the intro message
  intro()

  # Start the main program
  main()

  # Exit the application with a smile
  outro()