import logging
import logging.config

import pytoml as toml
import colorama
colorama.init()

COLORS = {
  'DEBUG': colorama.Fore.BLUE,
  'INFO': colorama.Fore.WHITE,
  'WARNING': colorama.Fore.YELLOW,
  'ERROR': colorama.Fore.RED,
  'CRITICAL': colorama.Fore.MAGENTA,
}


class ColorFormatter(logging.Formatter):
  """Class to add color to the logs."""

  def format(self, record):
    """Convert the log level name to a color version"""
    space = "\t\t" if record.levelname in ["INFO", "DEBUG"] else "\t"
    record.levelname = "[" + COLORS[record.levelname] + record.levelname  + colorama.Fore.RESET + "]" + space
    return super().format(record)


LOGGING_CONFIG = {
  'version': 1,
  'formatters': {
    'standard': {
      'format': '%(asctime)s %(levelname)s%(name)s: %(message)s'
    },
    'color': {
      'format': '%(asctime)s %(levelname)s%(name)s: %(message)s',
      'class': 'config.ColorFormatter'
    }
  },
  'handlers': {
    'default': {
      'level': 'INFO',
      'formatter': 'color',
      'class': 'logging.StreamHandler'
    }
  },
  'loggers': {
    'bot': {
      'handlers': ['default'],
      'level': 'DEBUG',
      'propagate': True
    },
    'bananabee': {
      'handlers': ['default'],
      'level': 'DEBUG',
      'propagate': True
    },
    # 'config': {
    #   'handlers': ['default'],
    #   'level': 'DEBUG',
    #   'propagate': True
    # }
  }
}

logging.config.dictConfig(LOGGING_CONFIG)

PERIOD = 2

PARAMS = dict()

# Load in the toml configuration
with open('config.toml') as fpointer:
  PARAMS = toml.load(fpointer)

LOGGER = logging.getLogger(__name__)

# Attempt to load override config
try:
  with open('config.override.toml') as fpointer:
    LOGGER.debug('Found override config file. Using that instead.')
    PARAMS.update(toml.load(fpointer))
except FileNotFoundError:
  pass
