import pyautogui

#Locations
stake_box_location = 1306, 527
call_button_location = 1732, 410
put_button_location = 1724, 569
contract_confirmation_exit_location = 1883, 363

pyautogui.PAUSE = 1
pyautogui.FAILSAFE = True

def find_mouse_coordinates():
    """Used to find mouse location (x,y) on a screen"""
    print(pyautogui.position())

#add to stake function
def add_amount_to_stake_box(stake_box_location):
    """clicks on the stake box and deletes all characters, then types in new stake amount"""
    pyautogui.moveTo(stake_box_location)
    pyautogui.click(stake_box_location)

    #deletes up to 10 characters incase BTC amount is used, example: 0.00000001
    pyautogui.typewrite(['backspace', 'backspace', 'backspace', 'backspace', 'backspace', 'backspace', 'backspace', 'backspace', 'backspace, backspace'])

    #add in stake amount from the stake calculations in the stake function
    pyautogui.typewrite('100')

#add to contract purchase function under CALL contract type
def call_click(call_button_location):
    """clicks on the Higher/Call purchase button"""
    pyautogui.moveTo(call_button_location)
    pyautogui.click(call_button_location)

#add to contract purchase function under PUT contract type
def put_click(put_button_location):
    """clicks on the Lower/Put purchase button"""
    pyautogui.moveTo(put_button_location)
    pyautogui.click(put_button_location)

#add to end of contract purchase call
def contract_confirmation_exit(contract_confirmation_exit_location):
    """clicks on the x to exit the contract confirmation"""
    pyautogui.moveTo(contract_confirmation_exit_location, duration=0.5)
    pyautogui.click(contract_confirmation_exit_location)

def main():
    call_click(call_button_location)
    contract_confirmation_exit(contract_confirmation_exit_location)


if (__name__ == '__main__'):
    main()
